const express = require('express');
const router = express.Router();
const {
  list,
  create,
  update,
  destroy,
  find,
} = require('../app/controllers/carController');
// const validate = require('../middleware/validate');
// const { createUserRules } = require('../validators/rule');

router.get('/list', (req, res) => list(req, res));
router.post('/create', create);
// router.post('/create', validate(createUserRules), create);
router.get('/find/:id', find);
router.post('/update/:id', update);
router.delete('/destroy/:id', destroy);
module.exports = router;
