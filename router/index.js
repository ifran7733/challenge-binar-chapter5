const express = require('express');
const router = express.Router();
const carRouter = require('./car');

router.get('/check', (req, res) => res.send('Aplication Up'));
router.use('/car', carRouter);

module.exports = router;
