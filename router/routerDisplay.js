const express = require('express');
const router = express.Router();
const carDisplayController = require('../app/controllers/carDisplayController');

router.get('/', carDisplayController.index);
router.get('/add', carDisplayController.add);
router.get('/update/:id', carDisplayController.edit);
router.get('/delete/:id', carDisplayController.delete);

module.exports = router;
