'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return await queryInterface.bulkInsert('cars', [
      {
        name: 'irfan',
        price: '2000',
        size: 'small',
        photo: 'tes',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'irfan',
        price: '2000',
        size: 'medium',
        photo: 'tes',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'irfan',
        price: '2000',
        size: 'large',
        photo: 'tes',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
