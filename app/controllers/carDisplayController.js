const axios = require('axios');

exports.index = (req, res) => {
  axios
    .get('http://localhost:3000/api/car/list')
    .then((resp) => {
      const data = {
        layout: 'layouts/main-layout',
        cars: resp.data,
        name: 'Irfan',
      };
      res.render('index', data);
    })
    .catch((error) => {
      console.log(error);
    });
};

exports.add = (req, res) => {
  res.render('add', { layout: 'layouts/main-layout' });
};

exports.edit = (req, res) => {
  const url = `http://localhost:3000/api/car/find/${req.params.id}`;
  axios
    .get(url)
    .then((resp) => {
      const { id, name, price, size, photo } = resp.data.data;
      const data = {
        layout: 'layouts/main-layout',
        id,
        name,
        price,
        size,
        photo,
      };
      res.render('edit', data);
    })
    .catch((error) => {
      console.log(error);
    });
};
exports.delete = (req, res) => {
  const url = `http://localhost:3000/api/car/find/${req.params.id}`;
  axios
    .get(url)
    .then((resp) => {
      const { id, name, price, size, photo } = resp.data.data;
      const data = {
        layout: 'layouts/main-layout',
        id,
        name,
        price,
        size,
        photo,
      };
      res.render('delete', data);
    })
    .catch((error) => {
      console.log(error);
    });
};
