const express = require('express');
require('dotenv').config();
const port = process.env.PORT || 3500;
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = require('./router');
const axios = require('axios');
const expressLayouts = require('express-ejs-layouts');

app.use(express.static(path.join(__dirname, '/public')));

app.set('view engine', 'ejs');
app.use(expressLayouts);

const routerDisplay = require('./router/routerDisplay');
app.use('/', routerDisplay);

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);

app.listen(port, () => {
  console.log(`server running at port ${port}`);
});
